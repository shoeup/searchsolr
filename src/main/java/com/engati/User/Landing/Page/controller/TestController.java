package com.engati.User.Landing.Page.controller;

import com.engati.User.Landing.Page.entity.ProductDetails;
import com.engati.User.Landing.Page.service.Imp.UserServiceImp;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TestController {

    @Autowired
    UserServiceImp userServiceimp;

    @CrossOrigin(origins = "http://172.16.28.138:3000")
    @GetMapping(value="/get")
    public ResponseEntity<List<ProductDetails>> getRes(@RequestParam(value = "productName") String productName, @RequestParam(value = "id") Long merchantId ) throws IOException, SolrServerException {
        ResponseEntity<List<ProductDetails>> responseEntity=new ResponseEntity(userServiceimp.getRes(productName,merchantId), HttpStatus.OK);

        return responseEntity;

    }


    @PostMapping(value = "/getvalue")
    public ResponseEntity<String> addProduct(@RequestBody ProductDetails p) throws IOException, SolrServerException {
        ResponseEntity<String> responseEntity = new ResponseEntity(userServiceimp.addProduct(p),HttpStatus.ACCEPTED);

        return responseEntity;
    }
//    @GetMapping("name")
//    public String getname(){
//        return "hello mate";
//    }

    @PostMapping(value="/delete")
    public ResponseEntity<String> del(@RequestParam(value = "pId") Long pId) throws IOException, SolrServerException {
        ResponseEntity<String> responseEntity=new ResponseEntity(userServiceimp.del(pId), HttpStatus.OK);

        return responseEntity;

    }
     @CrossOrigin(origins = "http://172.16.29.149:3000")
     @GetMapping(value="/getproduct")
     public ResponseEntity<List<ProductDetails>> getPUser(@RequestParam(value = "productName") String productName) throws IOException, SolrServerException {
         ResponseEntity<List<ProductDetails>> responseEntity = new ResponseEntity(userServiceimp.getPUser(productName), HttpStatus.OK);

         return responseEntity;
     }

}






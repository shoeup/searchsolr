package com.engati.User.Landing.Page.entity;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ProductDetails {

    @Field
    Long id;
    @Field
    Long categoryId;
    @Field
    String productName;
    @Field(value = "description")
    String description;
    @Field
    String productImage;
    @Field(value = "productId")
    Long productId;
    @Field
    Long staticId;
    @Field
    Long merchantId;
    @Field
    Long stock;
    @Field
    Long price;
    @Field
    Long size;
    @Field
    String color;


}

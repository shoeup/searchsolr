package com.engati.User.Landing.Page.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductUser {
    @Field
    Long id;

    @Field
    Long categoryId;

    @Field
    Long stock;

    @Field
    Long price;

    @Field
    String color;

    @Field
    Long staticId;

    @Field
    String productName;

    @Field
    String description;
    @Field
    String productImage;
}

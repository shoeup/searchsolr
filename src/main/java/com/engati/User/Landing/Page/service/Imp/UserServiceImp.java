package com.engati.User.Landing.Page.service.Imp;


import com.engati.User.Landing.Page.entity.ProductUser;
import com.engati.User.Landing.Page.entity.ProductDetails;
import com.engati.User.Landing.Page.entity.UpdateDynamic;
import com.engati.User.Landing.Page.service.UserService;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.stereotype.Service;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service

public class UserServiceImp implements UserService {



    final String solrUrl = "http://localhost:8983/solr";
    SolrClient client = new HttpSolrClient.Builder(solrUrl).build();
    @Override
    public String addProduct(ProductDetails p) throws IOException, SolrServerException{
        p.setProductName(p.getProductName().toLowerCase());
            UpdateResponse response = client.addBean("product_details", p);
        return "Added";
    }

    @Override
    public List<ProductDetails> getRes(String productName, Long merchantId) throws IOException, SolrServerException{

        String pLower = productName.toLowerCase();
        StringBuilder str = new StringBuilder("productName:");
        str.append("*");
        StringBuilder str1 = new StringBuilder(str);
        str1.append(pLower);
        StringBuilder str2 = new StringBuilder(str1);
        str2.append("*");
        StringBuilder str3 = new StringBuilder(str2);
        str3.append(" AND ");
        StringBuilder str4 = new StringBuilder(str3);
        str4.append("merchantId");
        StringBuilder str5 = new StringBuilder(str4);
        str5.append(":");
        StringBuilder str6 = new StringBuilder(str5);
        str6.append(merchantId);
        String s = str6.toString();

        SolrQuery query = new SolrQuery(s);
        QueryResponse queryResponse = client.query("product_details",query);

        List<ProductDetails> productDetailsList = queryResponse.getResults().stream().map(solrRecord -> {
            ProductDetails p = new ProductDetails();

            p.setId(Long.parseLong((String) solrRecord.get("id")));
            p.setCategoryId((Long) solrRecord.get("categoryId"));
            p.setProductName((String) solrRecord.get("productName"));
            p.setDescription((String) solrRecord.get("description"));
            p.setProductImage((String) solrRecord.get("productImage"));
            p.setProductId((Long) solrRecord.get("productId"));
            p.setStaticId((Long) solrRecord.get("staticId"));
            p.setMerchantId((Long) solrRecord.get("merchantId"));
            p.setStock((Long) solrRecord.get("stock"));
            p.setPrice((Long) solrRecord.get("price"));
            p.setSize((Long) solrRecord.get("size"));
            p.setColor((String) solrRecord.get("color"));
            return p;
        }).collect(Collectors.toList());

        return productDetailsList;
    }

    @Override
    public String del(Long productId) throws IOException, SolrServerException {
        String str = "id";
        String s = str+":"+ productId;

        UpdateResponse queryResponse = client.deleteByQuery("product_details",s);

        return "deleted";
    }
    @Override
    public List<ProductUser> getPUser(String productName) throws IOException, SolrServerException {

        String str = "productName:";

        String s = str + "*"+productName.toLowerCase()+"*";

        SolrQuery query = new SolrQuery(s);
        QueryResponse queryResponse = client.query("product_details",query);
        List<ProductUser> productUsers = queryResponse.getResults().stream().map(solrRecord -> {
            ProductUser p = new ProductUser();

            p.setId(Long.parseLong((String) solrRecord.get("id")));
            p.setCategoryId((Long) solrRecord.get("categoryId"));
            p.setProductName((String) solrRecord.get("productName"));
            p.setDescription((String) solrRecord.get("description"));
            p.setProductImage((String) solrRecord.get("productImage"));
            p.setStaticId((Long) solrRecord.get("staticId"));
            p.setStock((Long) solrRecord.get("stock"));
            p.setPrice((Long) solrRecord.get("price"));

            p.setColor((String) solrRecord.get("color"));
            return p;
        }).collect(Collectors.toList());

        return productUsers;
    }
//    public String update(UpdateDynamic d) throws IOException, SolrServerException {
//
//        String str = "id";
//        String s = str+":"+ d.getProductId();
//
//        SolrQuery query = new SolrQuery(s);
//
//        QueryResponse queryResponse = client.query("product_details",query);
//        List<ProductUser> productUsers = queryResponse.getResults().stream().map(solrRecord -> {
////            UpdateDynamic dynamic = new UpdateDynamic();
//
//            solrRecord.setField("staticId",);
//        }
//
//        return "updated";
//    }

}

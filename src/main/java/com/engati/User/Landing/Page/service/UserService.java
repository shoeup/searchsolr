package com.engati.User.Landing.Page.service;

import com.engati.User.Landing.Page.entity.ProductDetails;
import com.engati.User.Landing.Page.entity.ProductUser;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.List;

public interface UserService {

   public String addProduct(ProductDetails p) throws IOException, SolrServerException;
   public List<ProductDetails> getRes(String productName, Long merchantId) throws IOException, SolrServerException;
   public String del(Long productId) throws IOException, SolrServerException;
   public List<ProductUser> getPUser(String productName) throws IOException, SolrServerException;
}

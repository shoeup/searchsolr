package com.engati.User.Landing.Page.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDynamic {

    Long staticId;
    Long merchantId;
    Long productId;
    Long stock;
    Long price;
    Long size;
}
